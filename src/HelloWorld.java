import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JApplet;

public class HelloWorld extends JApplet implements Runnable {
	private boolean keepGoing;
	private Thread thread;
	private Color color;
	
	public void init() {
		keepGoing = true;
		
		thread = new Thread(this);
		
		thread.start();
	}
	
	public void stop() {
		keepGoing = false;
	}
	
	public void destroy() {
		keepGoing = false;
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		
		int size = 12;
		
		g.setFont(new Font("Comic Sans MS",Font.BOLD,size));
		
		FontMetrics metrics = g.getFontMetrics();
		
		int width = metrics.stringWidth("Hello World");
		
		while (width < getWidth()) {
			size++;
			
			g.setFont(new Font("Comic Sans MS",Font.BOLD,size));
			
			metrics = g.getFontMetrics();
			
			width = metrics.stringWidth("Hello World");
		}
		
		int height = metrics.getHeight();
		
		g.setColor(color);
		
		g.drawString("Hello World", getWidth()/2 - width/2, getHeight()/2+height/4);
	}
	
	private void pause(double seconds) {
		try {
			Thread.sleep((int)(seconds*1000));
		} catch (InterruptedException ie) {
			System.out.println(ie);;
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		while (keepGoing) {
			pause(0.5);
			
			int red = (int)(256*Math.random());
			int green = (int)(256*Math.random());
			int blue = (int)(256*Math.random());
			
			color = new Color(red,green,blue);
			
			repaint();			
		}
		
	}

}
