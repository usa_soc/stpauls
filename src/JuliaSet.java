import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class JuliaSet extends JApplet {
	   private FilledJuliaSetPanel juliaSet;
	   private JuliaSetControlPanel juliaSetControl;

	   private ComplexNumber f(ComplexNumber c1) {
	      double creal = juliaSetControl.creal();
	      double cimaginary = juliaSetControl.cimaginary();

	      ComplexNumber temp = c1.multiply(c1);

	      temp = temp.add(new ComplexNumber(creal,cimaginary));

	      return(temp);
	   }
	   

	   private JButton createJButton(String label,
	                                 int width,
	                                 int height,
	                                 int xlocation,
	                                 int ylocation,
	                                 ActionListener listener) {
	      JButton button = new JButton(label);
	      button.setSize(width,height);
	      button.setLocation(xlocation,ylocation);

	      Insets insets = button.getInsets();
	  
	      insets.left = 0;
	      insets.right = 0;

	      button.setMargin(insets);

	      button.addActionListener(listener);

	      return(button);
	   }

	   private JCheckBox createJCheckBox(String label,
	                                     int width,
	                                     int height,
	                                     int xlocation,
	                                     int ylocation,
	                                     ItemListener listener) {
	      JCheckBox box = new JCheckBox(label);
	      box.setSize(width,height);
	      box.setLocation(xlocation,ylocation);

	      box.addItemListener(listener);

	      return(box);
	   }

	   private JRadioButton createJRadioButton(String label,
	                                           ButtonGroup group,
	                                           Color color,
	                                           int width,
	                                           int height,
	                                           int xlocation,
	                                           int ylocation,
	                                           ItemListener listener) {
	      JRadioButton button = new JRadioButton(label);
	      button.setSize(width,height);
	      button.setLocation(xlocation,ylocation);

	      button.setBackground(color);

	      group.add(button);

	      button.addItemListener(listener);

	      return(button);
	   }

	   private JComboBox createJComboBox(String[] choices,
	                                     int width,
	                                     int height,
	                                     int xlocation,
	                                     int ylocation,
	                                     ItemListener listener) {
	      JComboBox box = new JComboBox(choices);
	      box.setSize(width,height);
	      box.setLocation(xlocation,ylocation);

	      box.addItemListener(listener);

	      return(box);
	   }

	   private JTextField createJTextField(int width,
	                                       int height,
	                                       int xlocation,
	                                       int ylocation) {
	      JTextField field = new JTextField();
	      field.setSize(width,height);
	      field.setLocation(xlocation,ylocation);

	      return(field);
	   }

	   private JLabel createJLabel(String text,
	                               int width,
	                               int height,
	                               int xlocation,
	                               int ylocation) {
	      JLabel label = new JLabel("<html><font color=#FFFFFF>" + text + "</font></html>");
	      label.setSize(width,height);
	      label.setLocation(xlocation,ylocation);
	      
	      return(label);
	   }

	   private void pause(double seconds) {
	      try {
	         Thread.sleep((int)(seconds*1000));
	      } catch (InterruptedException ie) {
	      }
	   }
	   
	   public class ComplexNumber {
	      private double realPart;
	      private double imaginaryPart;

	      public ComplexNumber(double realPart,
	                           double imaginaryPart) {
	         this.realPart = realPart;
	         this.imaginaryPart = imaginaryPart;
	      }

	// A getter method to return the real part.

	      public double realPart() {
	         return(realPart);
	      }

	// A getter method to return the imaginary part.

	      public double imaginaryPart() {
	         return(imaginaryPart);
	      }

	// An instance method which computes the magnitude
	// of the complex number.

	      public double magnitude() {
	         return(Math.sqrt(Math.pow(realPart,2) + Math.pow(imaginaryPart,2)));
	      }

	// An instance method which forms a new complex number
	// by adding the real and imaginary parts of the caller
	// of the method and the paramter. The sum is returned.

	      public ComplexNumber add(ComplexNumber c) {
	         double realPart = this.realPart() + c.realPart();
	         double imaginaryPart = this.imaginaryPart() + c.imaginaryPart();

	         return(new ComplexNumber(realPart,imaginaryPart));
	      }

	// An instance method which forms a new complex number
	// by multiplying the caller of the method with the parameter.
	// We use the simple rules of multiplication of polynomials.
	// If the first complex number is a+bi and the second complex
	// number is c+di, then the product is found as

	// (a+bi)(c+di) = ac + bci + adi + bdi*i = (ac - bd) + (bc + ad)i

	// We return the product.

	      public ComplexNumber multiply(ComplexNumber c) {
	         double realPart = this.realPart*c.realPart() - this.imaginaryPart*c.imaginaryPart();
	         double imaginaryPart = this.realPart*c.imaginaryPart() + this.imaginaryPart()*c.realPart();

	         return(new ComplexNumber(realPart,imaginaryPart));
	      }
	   }

	   public class MyPanel extends JPanel {
	      private Color color;

	      public MyPanel(int width,
	                     int height,
	                     int xlocation,
	                     int ylocation) {
	         super(true);

	         setSize(width,height);
	         setLocation(xlocation,ylocation);
	         
	         System.out.println("MyPanel");

	         color = Color.white;
	      }

	// A setter method to set the color of the panel. After the
	// color is set, the panel is repainted.

	      public void setColor(Color color) {
	         this.color = color;

	         repaint();
	      }

	// We simply clear out the panel, set the drawing color, and
	// then fill in the panel.

	      public void paint(Graphics g) {
	         g.clearRect(0,0,getWidth(),getHeight());

	         super.paint(g);

	         g.setColor(color);

	         g.fillRect(0,0,getWidth(),getHeight());
	      }
	   }
	   
	   public class FilledJuliaSetPanel extends JPanel implements Runnable {
	      private MyPanel[][] panels;

	// An instance method which will convert a Java x-coordinate into
	// a real x-coordinate.

	      private double actualX(int value) {
	         int upperx = juliaSetControl.upperx();
	         int uppery = juliaSetControl.uppery();
	         int lowerx = juliaSetControl.lowerx();
	         int lowery = juliaSetControl.lowery();

	         return((lowerx-upperx)/400.0*value+upperx);
	      }

	// An instance method which will convert a Java y-coordinate into
	// a real y-coordinate.

	      private double actualY(int value) {
	         int upperx = juliaSetControl.upperx();
	         int uppery = juliaSetControl.uppery();
	         int lowerx = juliaSetControl.lowerx();
	         int lowery = juliaSetControl.lowery();

	         return((lowery-uppery)/400.0*value+uppery);
	      }

	// Here we perform the major task associated with drawing the
	// filled Julia Set. We take the x and y coordinates, the real
	// and imaginary parts of a complex number, and form a ComplexNumber.
	// We then send the complex number to the function which computes
	// z^2 + c. We take the output of that method and check its magnitude.
	// If the magntide has gone beyond the value the user chose for
	// infinity, then we return the number of times we have called the
	// method. If we do this 15 times without going beyond infinity,
	// we assume that the magnitude will not go beyond infinity and
	// return the value indicating that the color black should be used.

	      public int value(double x,
	                       double y) {
	         ComplexNumber temp = new ComplexNumber(x,y);

	         for (int counter=1;counter<15;counter++) {
	            temp = f(temp);

	            double magnitude = temp.magnitude();

	            if (magnitude > juliaSetControl.infinity())
	               return(counter);
	         }

	// It is possible that the return will not be executed in the loop,
	// so we have to include this return statement.

	         return(15);
	      }

	// This thread definition will determine the colors of the panels
	// in a column. The column is sent to the Thread as a parameter.
	// We simply step through the column and convert each point in the
	// column to real coordinates, and determine the color of the panel
	// associated with a point as seen by Java.

	      public class MyThread extends Thread {
	         private int column;

	         public MyThread(int column) {
	            this.column = column;

	            start();
	         }

	         public void run() {
	            for (int counter=0;counter<getHeight();counter++) {
	               double x = actualX(column);
	               double y = actualY(counter);
	               
	               

	               panels[column][counter].setColor(juliaSetControl.color(value(x,y)));
	            }
	         }
	      }            

	      public FilledJuliaSetPanel(int width,
	                                 int height,
	                                 int xlocation,
	                                 int ylocation) {
	         super(true);

	         setSize(width,height);
	         setLocation(xlocation,ylocation);

	         setLayout(null);

	         setBackground(Color.black);

	// We make sure that the instance variable points to a real
	// two-dimensional array of MyPanels. Then we create each
	// one as a 1 x 1 panel. We add each small panel to the Filled
	// Julia Set panel to form the mosaic.

	      //   panels = new MyPanel[getWidth()][getHeight()];

	      //   for (int counter=0;counter<panels.length;counter++)
	      //      for (int counter1=0;counter1<panels[0].length;counter1++)
	      //         add(panels[counter][counter1] = new MyPanel(1,1,counter,counter1));
	         
	       }

	// When this method is called, the Filled Julia Set will be drawn.

	      public void fill() {
	  //       new Thread(this).start();
	    	  
	    	  repaint();
	    	  
	         
	      }
	      
	      public void paintComponent(Graphics g) {
	    	  super.paintComponent(g);
	    	  
	    	  for (int column=0;column<getWidth();column++)
	   	    	     for (int counter=0;counter<getHeight();counter++) {
		               double x = actualX(column);
		               double y = actualY(counter);
		               
		               g.setColor(juliaSetControl.color(value(x,y)));
		               
		               g.fillRect(column, counter, 1, 1);
		            }
	    	  
	    	  juliaSetControl.printMap();
	      }

	// A MyThread is created for each column, and each thread runs
	// independently.

	      public void run() {
	         for (int counter=0;counter<getWidth();counter++)
	            new MyThread(counter);
	      }
	   }
	   

	   public class JuliaSetControlPanel extends JPanel implements ActionListener,ItemListener {
	      private JComboBox red;
	      private JComboBox green;
	      private JComboBox blue;
	      private int[] redValues;
	      private int[] greenValues;
	      private int[] blueValues;
	      private JComboBox iterations;
	      private JButton store;
	      private JTextField upperx;
	      private JTextField uppery;
	      private JTextField lowerx;
	      private JTextField lowery;
	      private JTextField creal;
	      private JTextField cimaginary;
	      private JTextField infinity;
	      private JButton draw;
	      private Map<Integer,Integer> map;

	// We initialize the arrays containing the colors associated
	// with the iterations. We must be careful here. If we were
	// to create the array within the method, we would not be able
	// to affect the parameters.
	      
	      public void printMap() {
	    	  for (int number : map.keySet())
	    		  System.out.println(number + " " + map.get(number));
	      }

	      private void initialize(int[] red,
	                              int[] green,
	                              int[] blue) {
	         for (int counter=0;counter<red.length-1;counter++) {
	            red[counter] = 255-10*counter;
	            green[counter] = 0;
	            blue[counter] = 100+10*counter;
	         }

	         red[14] = 0;
	         green[14] = 0;
	         blue[14] = 0;

	         upperx.setText("-4");
	         uppery.setText("4");
	         lowerx.setText("4");
	         lowery.setText("-4");
	         creal.setText("-1");
	         cimaginary.setText("0");
	         infinity.setText("4");
	      }

	      public JuliaSetControlPanel(int width,
	                                  int height,
	                                  int xlocation,
	                                  int ylocation) {
	         super(true);

	         setSize(width,height);
	         setLocation(xlocation,ylocation);

	         setBackground(Color.blue);

	         setLayout(null);

	         String[] choices = new String[256];

	         for (int counter=0;counter<choices.length;counter++)
	            choices[counter] = counter + "";

	         add(createJLabel("Red",50,20,20,5));
	         add(red = createJComboBox(choices,50,20,70,5,this));
	         add(createJLabel("Green",50,20,140,5));
	         add(green = createJComboBox(choices,50,20,200,5,this));
	         add(createJLabel("Blue",50,20,270,5));
	         add(blue = createJComboBox(choices,50,20,330,5,this));

	         choices = new String[15];

	         for (int counter=0;counter<choices.length;counter++)
	            choices[counter] = (counter+1) + "";

	         add(createJLabel("Iterations",100,20,20,35));
	         add(iterations = createJComboBox(choices,50,20,130,35,this));

	         add(store = createJButton("Store",50,20,220,35,this));

	         redValues = new int[15];
	         greenValues = new int[15];
	         blueValues = new int[15];

	         red.setSelectedItem(redValues[0] + "");
	         green.setSelectedItem(greenValues[0] + "");
	         blue.setSelectedItem(blueValues[0] + "");

	         add(createJLabel("Upper X",100,20,20,60));
	         add(upperx = createJTextField(100,20,130,60));
	         add(createJLabel("Upper Y",100,20,20,80));
	         add(uppery = createJTextField(100,20,130,80));
	         add(createJLabel("Lower X",100,20,20,100));
	         add(lowerx = createJTextField(100,20,130,100));
	         add(createJLabel("Lower Y",100,20,20,120));
	         add(lowery = createJTextField(100,20,130,120));
	         add(createJLabel("C real",100,20,20,140));
	         add(creal = createJTextField(100,20,130,140));
	         add(createJLabel("C imaginary",100,20,20,160));
	         add(cimaginary = createJTextField(100,20,130,160));
	         add(createJLabel("Infinity",100,20,20,180));
	         add(infinity = createJTextField(100,20,130,180));

	         initialize(redValues,greenValues,blueValues);
	 
	         red.setSelectedItem(redValues[0] + "");
	         green.setSelectedItem(greenValues[0] + "");
	         blue.setSelectedItem(blueValues[0] + "");

	         add(draw = createJButton("Draw",100,100,getWidth()-125,getHeight()-125,this));
	         
	         map = new HashMap<>();
	      }

	      public int upperx() {
	         return(Integer.parseInt(upperx.getText()));
	      }

	      public int uppery() {
	         return(Integer.parseInt(uppery.getText()));
	      }

	      public int lowerx() {
	         return(Integer.parseInt(lowerx.getText()));
	      }

	      public int lowery() {
	         return(Integer.parseInt(lowery.getText()));
	      }

	      public double creal() {
	         return(Double.parseDouble(creal.getText()));
	      }

	      public double cimaginary() {
	         return(Double.parseDouble(cimaginary.getText()));
	      }

	      public double infinity() {
	         return(Double.parseDouble(infinity.getText()));
	      }

	      public Color color(int number) {
	    	  if (map.get(number) == null)
	    		  map.put(number, 1);
	    	  else {
	    		  int count = map.get(number);
	    		  
	    		  map.put(number, count+1);
	    	  }
	    	  
	    	
	         return(new Color(redValues[number-1],
	                          greenValues[number-1],
	                          blueValues[number-1]));
	      }

	// If the user clicks Draw, then we call the fill method in
	// the Filled Julia Set panel. If the user chooses store,
	// we store the current values in the JComboBoxes in the color
	// arrays.

	      public void actionPerformed(ActionEvent e) {
	         if (e.getSource() == draw)
	            juliaSet.fill();
	         else if (e.getSource() == store) {
	            int number = Integer.parseInt(iterations.getSelectedItem().toString());

	            redValues[number-1] = Integer.parseInt(red.getSelectedItem().toString());
	            greenValues[number-1] = Integer.parseInt(green.getSelectedItem().toString());
	            blueValues[number-1] = Integer.parseInt(blue.getSelectedItem().toString());
	         }
	      }

	// Whenever the user selects one of the iterations, then
	// show the color associated with the iteration.

	      public void itemStateChanged(ItemEvent e) {
	         if (e.getSource() == iterations && e.getStateChange() == ItemEvent.SELECTED) {
	            int iteration = Integer.parseInt(iterations.getSelectedItem().toString());

	            red.setSelectedItem(redValues[iteration-1] + "");
	            green.setSelectedItem(greenValues[iteration-1] + "");
	            blue.setSelectedItem(blueValues[iteration-1] + "");
	         }
	      }
	   }
	   
	   public void init() {
		      Container container = getContentPane();

		      container.setLayout(null);

		      container.setBackground(Color.white);

		      container.add(juliaSet = new FilledJuliaSetPanel(400,400,0,0));
		      container.add(juliaSetControl = new JuliaSetControlPanel(400,200,0,400));
		   }

}
